import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Rest } from '../../providers/rest';
import { LoginPage } from '../login/login';
import { InAppBrowser } from '@ionic-native/in-app-browser';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
   

	countries: string[];
    errorMessage: string;
    tabBarElement: any;
  constructor(public navCtrl: NavController,public rest: Rest,private iab: InAppBrowser) {
  	this.tabBarElement = document.querySelector('#tabs ion-tabbar-section');

  	
  	//this.tabBarElement.style.display = 'none';

  }
  

  ionViewDidLoad() {
  	
  
   }
   onPageDidEnter()
    {

        this.tabBarElement.style.display = 'none';

    }

    onPageWillLeave()
    {

        this.tabBarElement.style.display = 'block';

    }

  loginpage(){

  	this.navCtrl.push(LoginPage);
  }
  signup(){

  	this.iab.create('http://beta.brstdev.com/PrizeFactory/webpage/signup.html','_self',{location:'no'}); 


  }

}
