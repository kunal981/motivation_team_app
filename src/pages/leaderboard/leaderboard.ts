import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
@Component({
  selector: 'page-leaderboard',
  templateUrl: 'leaderboard.html'
})
export class LeaderboardPage {
    constructor(public navCtrl: NavController) {
  }
   homepage(){
       this.navCtrl.push(HomePage);
   }

}
