import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ForgotPage } from '../forgot/forgot';
import { DashboardPage } from '../dashboard/dashboard';
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

	tabBarElement: any;
	title: any;

  constructor(public navCtrl: NavController) {
  	  this.title = 'abgular 4 with jquery';
  	  this.toggleTitle();
	  
  	this.tabBarElement = document.querySelector('.tabbar.show-tabbar');


  }
  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }
   homepage(){
     	this.navCtrl.push(HomePage);
   }
   forgotpage(){
      	this.navCtrl.push(ForgotPage);
   }
   dashboardpage(){
   	this.navCtrl.push(DashboardPage);
   }

   toggleTitle(){
	    $('.title').slideToggle(); //
	  }


}
