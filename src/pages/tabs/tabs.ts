import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { DashboardPage } from '../dashboard/dashboard';
import { RoutinePage } from '../routine/routine';
import { MessagePage } from '../message/message';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = DashboardPage;
  tab3Root = MessagePage;

  constructor() {

  }
}
