import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Rest } from '../../providers/rest';
import { LoginPage } from '../login/login';
import { ProfilePage } from '../profile/profile';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

  constructor(public navCtrl: NavController,public rest: Rest,private iab: InAppBrowser) {


  }

  profilepage(){

    this.navCtrl.push(ProfilePage);
  }
  

 
}
