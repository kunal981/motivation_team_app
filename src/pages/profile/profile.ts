import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ForgotPage } from '../forgot/forgot';
import { DashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})

export class ProfilePage {

	tabBarElement: any;
	title: any;
	hideelement: any;

	constructor(public navCtrl: NavController) {

		this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
		this.hideelement   = document.querySelector('.team');


	}

	ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
    this.hideelement.style.display = 'none';
  }
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
    this.hideelement.style.display = 'flex';
  }

  dashboardpage(){

  	this.navCtrl.push(DashboardPage);
  }




}
